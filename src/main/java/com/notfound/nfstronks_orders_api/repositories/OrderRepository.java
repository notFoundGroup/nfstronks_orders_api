package com.notfound.nfstronks_orders_api.repositories;

import com.notfound.nfstronks_orders_api.models.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {
    Optional<Order> findByProductId(Long productId);
    Page<Order> findAllByUserId(Long userId, Pageable pageable);
}
