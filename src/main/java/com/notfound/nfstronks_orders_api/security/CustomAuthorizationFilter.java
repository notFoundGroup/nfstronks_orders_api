package com.notfound.nfstronks_orders_api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notfound.nfstronks_orders_api.dto.TokenDTO;
import com.notfound.nfstronks_orders_api.dto.UsernamePasswordAuthenticationTokenDTO;
import com.notfound.nfstronks_orders_api.exceptions.ErrorMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static java.util.Arrays.stream;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
public class CustomAuthorizationFilter extends OncePerRequestFilter {
    public static final String PREFIX_ATTRIBUTE = "Bearer ";

    RestTemplate restTemplate = new RestTemplate();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);

        log.info("Verifying headers...");
        if (authorizationHeader == null || !authorizationHeader.startsWith(PREFIX_ATTRIBUTE)) {
            filterChain.doFilter(request, response);
            return;
        }

        try {
            String token = authorizationHeader.replace(PREFIX_ATTRIBUTE, "");

            TokenDTO tokenDTO = new TokenDTO(token);
            URI uri = new URI(System.getenv("API_AUTH_URL") + "/auth/authorize");
            HttpMethod method = HttpMethod.POST;

            RequestEntity<TokenDTO> requestEntity = RequestEntity.post(uri).body(tokenDTO);
            log.info("Starting request to authenticationAPI: " + requestEntity);

            ResponseEntity<UsernamePasswordAuthenticationTokenDTO> responseEntity = restTemplate
                    .exchange(requestEntity, UsernamePasswordAuthenticationTokenDTO.class);
            HttpStatus status = responseEntity.getStatusCode();

            if (status.value() != 200) {
                log.error("Token rejected.");
                filterChain.doFilter(request, response);
                return;
            }

            log.info("Receiving body...");
            UsernamePasswordAuthenticationTokenDTO usernamePasswordAuthenticationTokenDTO = responseEntity.getBody();

            if (usernamePasswordAuthenticationTokenDTO == null) return;

            Collection<SimpleGrantedAuthority> roles = new ArrayList<>();

            stream(usernamePasswordAuthenticationTokenDTO.getRolesString()).
                    forEach(role -> roles.add(new SimpleGrantedAuthority(role)));

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(usernamePasswordAuthenticationTokenDTO.getPrincipal(),
                            usernamePasswordAuthenticationTokenDTO.getCredentials(),
                            roles);

            log.info("Creating Context for user: " + usernamePasswordAuthenticationToken.getPrincipal());
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

            filterChain.doFilter(request, response);
        } catch (URISyntaxException | HttpClientErrorException | HttpServerErrorException e) {
            log.error(e.getMessage());
            filterChain.doFilter(request, response);
        } catch (Exception e){
            log.error(Arrays.toString(e.getStackTrace()));
            response.setContentType(APPLICATION_JSON_VALUE);
            response.setStatus(500);
            ErrorMessage errorMessage = new ErrorMessage(500, new Date(), e.getMessage(), request.getServletPath());
            new ObjectMapper().writeValue(response.getOutputStream(), errorMessage);
        }
    }
}
