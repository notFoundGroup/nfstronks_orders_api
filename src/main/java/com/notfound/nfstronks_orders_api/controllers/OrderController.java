package com.notfound.nfstronks_orders_api.controllers;

import com.notfound.nfstronks_orders_api.dto.OrderDTO;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.*;
import com.notfound.nfstronks_orders_api.models.Order;
import com.notfound.nfstronks_orders_api.services.interfaces.OrderService;
import com.notfound.nfstronks_orders_api.services.kafka.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.Date;

import static com.notfound.nfstronks_orders_api.util.ProjectUtil.BASE_URL;
import static com.notfound.nfstronks_orders_api.util.ProjectUtil.stackTracingToLog;

@RestController
@CrossOrigin("*")
@RequestMapping(BASE_URL)
public class OrderController {
    @Autowired
    public OrderService service;

    @PostMapping
    public ResponseEntity<Order> register(@RequestBody OrderDTO newOrderDTO) {
        try {
            Order order = service.register(newOrderDTO);

            return ResponseEntity.ok(order);
        } catch (URISyntaxException | ProductNotAvailableException | ProductNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @PatchMapping("/confirmOrder/{id}")
    public ResponseEntity<Order> update(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(service.updateStatusToCompleted(id));
        } catch(OrderNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<Page<Order>> getAll(Pageable pageable) {
        try {
            return ResponseEntity.ok(service.getAll(pageable));
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> getById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(service.getById(id));
        } catch (OrderNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/currentUser")
    public ResponseEntity<Page<Order>> getAllByCurrentUserId(Pageable pageable) {
        try {
            Long id = Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString());

            return ResponseEntity.ok(service.getAllByUserId(id, pageable));
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<Page<Order>> getAllByUserId(@PathVariable Long userId, Pageable pageable) {
        try {
            return ResponseEntity.ok(service.getAllByUserId(userId, pageable));
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
