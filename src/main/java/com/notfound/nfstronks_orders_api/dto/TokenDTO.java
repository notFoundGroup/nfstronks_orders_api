package com.notfound.nfstronks_orders_api.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@RequiredArgsConstructor
@Getter
@Setter
public class TokenDTO {
    @NotEmpty
    private String token;

    public TokenDTO(String token) {
        this.token = token;
    }
}
