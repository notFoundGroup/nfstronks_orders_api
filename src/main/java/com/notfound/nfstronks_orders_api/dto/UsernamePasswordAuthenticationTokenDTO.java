package com.notfound.nfstronks_orders_api.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class UsernamePasswordAuthenticationTokenDTO {
    Object principal;
    Object credentials;
    String[] rolesString;
}