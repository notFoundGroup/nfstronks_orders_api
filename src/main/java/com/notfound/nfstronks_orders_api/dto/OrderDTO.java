package com.notfound.nfstronks_orders_api.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class OrderDTO {
    @NotNull
    private Date date;

    @NotEmpty
    private String status;

    private Long userId;

    @NotNull
    private Long productId;
}
