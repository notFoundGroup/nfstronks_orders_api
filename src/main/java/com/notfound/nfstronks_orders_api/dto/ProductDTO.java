package com.notfound.nfstronks_orders_api.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
public class ProductDTO {
    @NotEmpty
    private String title;

    @NotEmpty
    private String author;

    @NotEmpty
    private String description;

    @NotEmpty
    private String country;

    @NotNull
    private Date launchDate;

    @NotNull
    private Boolean isAvailable;

    @NotNull
    private Double price;
}

