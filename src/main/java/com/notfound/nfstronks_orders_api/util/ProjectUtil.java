package com.notfound.nfstronks_orders_api.util;

import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
public class ProjectUtil {
    public static final String BASE_URL = "/orders";

    public static final String KAFKA_TOPIC = System.getenv("KAFKA_TOPIC");

    public static final String KAFKA_URL = System.getenv("KAFKA_URL") + ":" + System.getenv("KAFKA_PORT");

    public static void stackTracingToLog(Exception e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stackTraceAsString = sw.toString();
        log.error(stackTraceAsString);
    }
}