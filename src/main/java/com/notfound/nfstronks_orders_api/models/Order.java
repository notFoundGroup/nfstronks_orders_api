package com.notfound.nfstronks_orders_api.models;

import com.notfound.nfstronks_orders_api.dto.OrderDTO;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tb_orders")
@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Order {

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_date", nullable = false)
    private Date date;

    @Column(name = "order_status", nullable = false, length = 10)
    private String status;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "product_id", nullable = false, unique = true)
    private Long productId;

    public void orderFromOrderDTO(OrderDTO orderDTO) {
        this.date = orderDTO.getDate() != null ? orderDTO.getDate() : this.date;
        this.status = orderDTO.getStatus() != null ? orderDTO.getStatus() : this.status;
        this.userId = orderDTO.getUserId() != null ? orderDTO.getUserId() : this.userId;
        this.productId = orderDTO.getProductId() != null ? orderDTO.getProductId() : this.productId;
    }
}
