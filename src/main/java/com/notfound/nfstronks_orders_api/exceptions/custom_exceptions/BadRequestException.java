package com.notfound.nfstronks_orders_api.exceptions.custom_exceptions;

public class BadRequestException extends RuntimeException {

    public BadRequestException(String message){
        super(message);
    }
}
