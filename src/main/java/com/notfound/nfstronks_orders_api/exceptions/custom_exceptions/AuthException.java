package com.notfound.nfstronks_orders_api.exceptions.custom_exceptions;

public class AuthException extends RuntimeException{

    public AuthException(String ErrorMessage){
        super(ErrorMessage);
    }
}