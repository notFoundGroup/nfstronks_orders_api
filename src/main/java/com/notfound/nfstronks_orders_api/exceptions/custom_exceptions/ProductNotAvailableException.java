package com.notfound.nfstronks_orders_api.exceptions.custom_exceptions;

public class ProductNotAvailableException extends Exception {
    public ProductNotAvailableException() {
        super("Produto não disponível.");
    }
}
