package com.notfound.nfstronks_orders_api.exceptions.custom_exceptions;

public class OrderNotFoundException extends Exception{
	
	public OrderNotFoundException() {
		super("Pedido não encontrado.");
	}

}