package com.notfound.nfstronks_orders_api.services.impl;

import com.notfound.nfstronks_orders_api.dto.OrderDTO;
import com.notfound.nfstronks_orders_api.dto.ProductDTO;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.OrderNotFoundException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotAvailableException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_orders_api.models.Order;
import com.notfound.nfstronks_orders_api.repositories.OrderRepository;
import com.notfound.nfstronks_orders_api.services.interfaces.OrderService;
import com.notfound.nfstronks_orders_api.services.kafka.KafkaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
@Primary
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository repository;

    RestTemplate restTemplate = new RestTemplate();

    @Override
    public Order register(OrderDTO newOrderDTO) throws ProductNotAvailableException, ProductNotFoundException,
            URISyntaxException, ExecutionException, InterruptedException {
        Long userId = Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString());

        newOrderDTO.setUserId(userId);
        newOrderDTO.setDate(new Date());
        newOrderDTO.setStatus("pending");

        log.info("Creating new order from the request body...");
        Order newOrder = new Order();
        newOrder.orderFromOrderDTO(newOrderDTO);

        log.info("Checking if there is an order for the same product...");
        Optional<Order> existsByProductId = repository.findByProductId(newOrder.getProductId());

        if (existsByProductId.isPresent()) {
            log.error("The requested product is unavailable.");
            throw new ProductNotAvailableException();
        }

        log.info("Sending request to check if the product exists within the database...");
        URI uri = new URI(System.getenv("API_PRODUCT_URL") + "/" + newOrder.getProductId());

        log.info(uri.toString());

        RequestEntity<ProductDTO> requestProduct = new RequestEntity<ProductDTO>(HttpMethod.GET, uri);

        try {
            ResponseEntity<ProductDTO> responseProduct = restTemplate
                    .exchange(requestProduct, ProductDTO.class);
        } catch (RestClientException ex) {
            log.error("The requested product doesn't exist.");
            throw new ProductNotFoundException();
        }

        log.info("Order sent.");

        newOrder = repository.save(newOrder);

        KafkaService.sendOrder(newOrder);

        return newOrder;
    }

    @Override
    public Order updateStatusToCompleted(Long id) throws OrderNotFoundException {
        Order order = this.getById(id);
        order.setStatus("completed");

        return repository.save(order);
    }

    @Override
    public void delete(Long id) throws OrderNotFoundException {
        boolean orderExists = repository.existsById(id);

        if (!orderExists) {
            throw new OrderNotFoundException();
        }

        repository.deleteById(id);
    }

    @Override
    public Page<Order> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Order getById(Long id) throws OrderNotFoundException {
        Optional<Order> order = repository.findById(id);

        if (order.isEmpty()) {
            throw new OrderNotFoundException();
        }

        return order.get();
    }

    public Page<Order> getAllByUserId(Long userId, Pageable pageable) {
        return repository.findAllByUserId(userId, pageable);
    }
}
