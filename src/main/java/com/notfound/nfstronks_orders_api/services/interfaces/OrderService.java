package com.notfound.nfstronks_orders_api.services.interfaces;

import com.notfound.nfstronks_orders_api.dto.OrderDTO;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.OrderNotFoundException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotAvailableException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_orders_api.models.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

public interface OrderService {
    Order register(OrderDTO newOrderDTO) throws ProductNotAvailableException, ProductNotFoundException, URISyntaxException, ExecutionException, InterruptedException;

    Order updateStatusToCompleted(Long id) throws OrderNotFoundException;

    void delete(Long id) throws OrderNotFoundException;

    Page<Order> getAll(Pageable pageable);

    Order getById(Long id) throws OrderNotFoundException;

    Page<Order> getAllByUserId(Long userId, Pageable pageable);
}
