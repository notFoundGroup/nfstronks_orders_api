package com.notfound.nfstronks_orders_api.services.kafka;

import com.google.gson.Gson;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.InternalServerErrorException;
import com.notfound.nfstronks_orders_api.models.Order;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.concurrent.ExecutionException;

import static com.notfound.nfstronks_orders_api.util.ProjectUtil.KAFKA_TOPIC;

public class KafkaService {
    public static void sendOrder(Order order) throws InterruptedException, ExecutionException {
        String key = "order";

        Gson gson = new Gson();
        String jsonOrderDTO = gson.toJson(order);

        KafkaProducer<String,String> producer = new KafkaProducer<String,String>(KafkaProperties.properties());
        ProducerRecord<String, String> record = new ProducerRecord<String, String>(KAFKA_TOPIC, key, jsonOrderDTO);

        Callback callback = (data, ex) -> {
            if (ex != null) {
                throw new InternalServerErrorException(ex.getMessage());
            }
        };

        producer.send(record, callback).get();
        producer.close();
    }
}
