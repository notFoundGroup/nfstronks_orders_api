package com.notfound.nfstronks_orders_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@SpringBootApplication
public class SitenameOrdersApi {
	public static void main(String[] args) {
		SpringApplication.run(SitenameOrdersApi.class, args);
	}
}