package com.notfound.nfstronks_orders_api.unit_tests;

import com.notfound.nfstronks_orders_api.controllers.OrderController;
import com.notfound.nfstronks_orders_api.dto.OrderDTO;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.OrderNotFoundException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotAvailableException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_orders_api.models.Order;
import com.notfound.nfstronks_orders_api.repositories.OrderRepository;
import com.notfound.nfstronks_orders_api.services.interfaces.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static testUtils.OrdersForTest.orderOK;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("Orders Integration Tests")
@AutoConfigureTestDatabase
class OrderControllerTest {
    @InjectMocks
    OrderController orderController;

    @Mock
    OrderService orderService;

    Authentication authentication = mock(Authentication.class);

    SecurityContext securityContext = mock(SecurityContext.class);

    @BeforeEach
    void setUp() {
        SecurityContextHolder.setContext(securityContext);
        BDDMockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        BDDMockito.when(securityContext.getAuthentication().getCredentials()).thenReturn("1");
    }

    @Test
    @DisplayName("GetAll calls service.getAll()")
    void getAll_callsServiceGetAll_whenSuccessful() {
        Pageable pageable = PageRequest.of(1, 10);

        ResponseEntity<Page<Order>> ordersPage = orderController.getAll(pageable);

        Mockito.verify(orderService, times(1)).getAll(pageable);
    }

    @Test
    @DisplayName("GetById calls service.getById()")
    void getById_callsServiceGetById_Always() throws OrderNotFoundException {
        ResponseEntity<Order> orderReturned = orderController.getById(1L);

        Mockito.verify(orderService, times(1)).getById(1L);
    }

    @Test
    @DisplayName("GetAllByUserId calls service.getAllByUserId()")
    void getAllByUserId_callsServiceGetAllByCurrentUserId_Always() {
        Pageable pageable = PageRequest.of(1, 10);

        ResponseEntity<Page<Order>> ordersPage = orderController.getAllByUserId(1L, pageable);

        Mockito.verify(orderService, times(1)).getAllByUserId(1L, pageable);
    }

    @Test
    @DisplayName("GetAllByCurrentUserId calls service.getAllByUserId()")
    void getAllByCurrentUserId_callsServiceGetAllByCurrentUserId_Always() {
        Pageable pageable = PageRequest.of(1, 10);

        ResponseEntity<Page<Order>> ordersPage = orderController.getAllByCurrentUserId(pageable);

        Mockito.verify(orderService, times(1)).getAllByUserId(1L, pageable);
    }

    @Test
    @DisplayName("Register calls service.register()")
    void register_callsServiceRegister() throws URISyntaxException, ProductNotAvailableException,
            ProductNotFoundException, ExecutionException, InterruptedException {
        OrderDTO orderOK = orderOK();

        ResponseEntity<Order> orderReturned = orderController.register(orderOK);

        Mockito.verify(orderService, times(1)).register(orderOK);
    }

    @Test
    @DisplayName("Update calls service.updateStatusToCompleted()")
    void update_callsServiceUpdateStatusToCompleted() throws OrderNotFoundException {
        ResponseEntity<Order> orderReturned = orderController.update(1L);

        Mockito.verify(orderService, times(1)).updateStatusToCompleted(1L);
    }
}