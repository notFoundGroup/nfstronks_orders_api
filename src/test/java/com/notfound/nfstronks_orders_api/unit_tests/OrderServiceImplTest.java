package com.notfound.nfstronks_orders_api.unit_tests;

import com.notfound.nfstronks_orders_api.dto.OrderDTO;
import com.notfound.nfstronks_orders_api.dto.ProductDTO;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.OrderNotFoundException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotAvailableException;
import com.notfound.nfstronks_orders_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_orders_api.models.Order;
import com.notfound.nfstronks_orders_api.repositories.OrderRepository;
import com.notfound.nfstronks_orders_api.services.impl.OrderServiceImpl;

import com.notfound.nfstronks_orders_api.services.kafka.KafkaService;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.common.serialization.StringSerializer;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static testUtils.OrdersForTest.*;

@ExtendWith(SpringExtension.class)
@DisplayName("Orders Unit Tests")
class OrderServiceImplTest {
    @InjectMocks
    OrderServiceImpl orderService;

    @InjectMocks
    KafkaService kafkaService;

    @Mock
    OrderRepository repositoryMocked;

    @Mock
    RestTemplate restTemplate;

    Authentication authentication = mock(Authentication.class);

    SecurityContext securityContext = mock(SecurityContext.class);

    MockProducer<String, String> mockProducer;

    @BeforeEach
    void setUp() {
        Order orderWithAllFields = orderWithAllFields();

        ProductDTO productDTOFull = productDTOFULL();
        Page<Order> page = mock(Page.class);

        SecurityContextHolder.setContext(securityContext);
        BDDMockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        BDDMockito.when(securityContext.getAuthentication().getCredentials()).thenReturn("1");

        BDDMockito.when(restTemplate.exchange(ArgumentMatchers.any(), ArgumentMatchers.any(Class.class)))
                .thenReturn(new ResponseEntity<>(productDTOFull, HttpStatus.OK));

        BDDMockito.when(repositoryMocked.save(ArgumentMatchers.any())).thenReturn(orderWithAllFields);

        BDDMockito.when(repositoryMocked.existsById(ArgumentMatchers.any())).thenReturn(true);

        BDDMockito.when(repositoryMocked.findAll(ArgumentMatchers.isA(Pageable.class))).thenReturn(page);

        BDDMockito.when(repositoryMocked.findById(ArgumentMatchers.any())).thenReturn(Optional.of(orderWithAllFields));

        BDDMockito.when(repositoryMocked.findByProductId(ArgumentMatchers.any())).thenReturn(Optional.empty());

        BDDMockito.when(repositoryMocked.findAllByUserId(ArgumentMatchers.any(), ArgumentMatchers.isA(Pageable.class))).thenReturn(page);
    }

    @Test
    @DisplayName("GetById returns a Order when successful")
    void getById_returnsOrder_whenSuccessful() throws OrderNotFoundException {
        Order orderWithAllFields = orderWithAllFields();

        Order orderReturned = orderService.getById(1L);

        Assertions.assertThat(orderReturned).isNotNull();
        Assertions.assertThat(orderReturned.getId()).isEqualTo(orderWithAllFields.getId());
    }

    @Test
    @DisplayName("GetById throws OrderNotFoundEx when Order is not found")
    void getById_throwsException_whenOrderNotFound() throws OrderNotFoundException {
        BDDMockito.when(repositoryMocked.findById(ArgumentMatchers.any())).thenReturn(Optional.empty());

        Assertions.assertThatThrownBy(() -> orderService.getById(1L)).isInstanceOf(OrderNotFoundException.class);
    }

    @Test
    @DisplayName("GetAll returns Page.class when successful")
    void getAll_returnsPage_whenSuccessful() {
        Pageable pageable = PageRequest.of(0, 10);

        Page<Order> ordersPage = orderService.getAll(pageable);

        Assertions.assertThat(ordersPage).isInstanceOf(Page.class);
    }

    @Test
    @DisplayName("GetAllByUserId returns Page.class when successful")
    void getAllByUserId_returnsPage_whenSuccessful() {
        Pageable pageable = PageRequest.of(0, 10);

        Page<Order> ordersPageForUser = orderService.getAllByUserId(1L, pageable);

        Assertions.assertThat(ordersPageForUser).isInstanceOf(Page.class);
    }

    @Test
    @DisplayName("Update returns Order when successful")
    void update_returnsOrder_whenSuccessful() throws OrderNotFoundException {
        Order orderWithAllFields = orderWithAllFields();

        Order orderReturned = orderService.updateStatusToCompleted(1L);

        Mockito.verify(repositoryMocked, times(1)).findById(1L);

        Assertions.assertThat(orderReturned.getStatus()).isEqualTo("completed");
        Assertions.assertThat(orderWithAllFields.getId()).isEqualTo(orderReturned.getId());
    }

    @Test
    @DisplayName("Delete calls repository.deleteById() when successful")
    void delete_callsRepositoryDelete_whenSuccessful() throws OrderNotFoundException {
        orderService.delete(1L);

        Mockito.verify(repositoryMocked, times(1)).deleteById(1L);
    }

    @Test
    @DisplayName("Delete throws OrderNotFoundException when Order is not found")
    void delete_throwsException_whenOrderNotFound() throws OrderNotFoundException {
        BDDMockito.when(repositoryMocked.existsById(ArgumentMatchers.any())).thenReturn(false);

        Assertions.assertThatThrownBy(() -> orderService.delete(1L)).isInstanceOf(OrderNotFoundException.class);
    }

//    @Test
//    @DisplayName("Register returns Order when successful")
//    void register_returnsOrder_whenSuccessful() throws URISyntaxException, ProductNotAvailableException,
//            ProductNotFoundException, ExecutionException, InterruptedException {
//        OrderDTO orderOK = orderOK();
//        Order orderWithAllFields = orderWithAllFields();
//
//        mockProducer = new MockProducer<>(true, new StringSerializer(), new StringSerializer());
//
//        Order orderReturned = orderService.register(orderOK);
//
//        Assertions.assertThat(orderReturned).isInstanceOf(Order.class);
//    }

    @Test
    @DisplayName("Register throws ProductNotAvailableEx when Product is not available")
    void register_throwsException_whenProductNotAvailable() throws URISyntaxException, ProductNotAvailableException,
            ProductNotFoundException {
        Order orderWithAllFields = orderWithAllFields();
        BDDMockito.when(repositoryMocked.findByProductId(ArgumentMatchers.any())).thenReturn(Optional.of(orderWithAllFields));

        OrderDTO orderOK = orderOK();

        Assertions.assertThatThrownBy(() -> orderService.register(orderOK)).isInstanceOf(ProductNotAvailableException.class);
    }

    @Test
    @DisplayName("Register throws ProductNotFoundEx when Product is not found")
    void register_throwsException_whenProductNotFound() throws URISyntaxException, ProductNotAvailableException,
            ProductNotFoundException {
        BDDMockito.when(restTemplate.exchange(ArgumentMatchers.any(), ArgumentMatchers.any(Class.class)))
                .thenThrow(RestClientException.class);

        OrderDTO orderOK = orderOK();

        Assertions.assertThatThrownBy(() -> orderService.register(orderOK)).isInstanceOf(ProductNotFoundException.class);
    }
}