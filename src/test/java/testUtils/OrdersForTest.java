package testUtils;

import com.notfound.nfstronks_orders_api.dto.OrderDTO;
import com.notfound.nfstronks_orders_api.dto.ProductDTO;
import com.notfound.nfstronks_orders_api.models.Order;

import java.util.Date;

public class OrdersForTest {
    public static OrderDTO orderOK() {
        return OrderDTO.builder()
                .status("pending")
                .date(new Date())
                .userId(1L)
                .productId(13L).build();
    }

    public static Order orderWithAllFields() {
        return Order.builder()
                .id(1L)
                .status("pending")
                .date(new Date())
                .userId(1L)
                .productId(1L).build();
    }

    public static OrderDTO orderWithCompletedStatus() {
        return OrderDTO.builder()
                .status("completed")
                .date(new Date())
                .userId(1L)
                .productId(1L).build();
    }

    public static ProductDTO productDTOFULL(){
        return ProductDTO.builder()
                .author("Henrique Lima")
                .country("BR")
                .description("Obra boa")
                .price(100d)
                .launchDate(new Date())
                .title("Obra Linda")
                .isAvailable(false).build();
    }
}
